
if(Drupal.jsEnabled){
	$(document).ready(function(){
		$('#block-outline_dmenu-0 .content li span.collapsed').click(expandAJAX);
		$('#block-outline_dmenu-0 .content li span.expanded').click(collapse);
		//$('#block-outline_dmenu-0 .content li a').click(link);
	});
}else{
	uri = "/outline_dmenu/get_static_menu/1781";
	$.ajax({
		type:"GET",
		url: uri,
		success:function(data){
			$('#block-outline_dmenu-0').empty();
			$('#block-outline_dmenu-0').append(data);
		}
	});
}

function expandAJAX(){
//$('#block-outline_dmenu-0 .content li.collapsed').click(function(){
		var nid = $(this).attr('title');
		//var basePath = Drupal.settings.ahah.basePaths['base'];
		var uri = '/outline_dmenu/update/'+nid;
		var data = 'nid='+nid;
		var curr_element = this;
		var curr_id = $(this).attr('title');
		
		$.ajax({
			type:"POST",
			url: uri,
			data: data,
			success:function(data){
				$('#dmenu_'+nid).append(data);
				$('#dmenu_'+nid+' > ul').hide();
				$('#dmenu_'+nid+' > ul').show('slow');
				
				$(curr_element).unbind('click',expandAJAX);
				$(curr_element).click(collapse);
				
				$(curr_element).removeClass('collapsed');
				$(curr_element).addClass('expanded');
				$('#dmenu_'+curr_id+' > ul li span.collapsed').click(expandAJAX);
				//$('#'+curr_id+' > ul li span.expanded').click(collapse);
				//$('#'+curr_id+' > ul li a').click(link);
				
			}
		});
		return false;
//	})	
}

function collapse(){
	var id=$(this).attr('title');
	$('#dmenu_'+id+' > ul').hide('slow');

	$(this).unbind('click',collapse);
	$(this).click(simpleExpand);
	
	$(this).removeClass('expanded');
	$(this).addClass('collapsed');
	
	return false;
}

function simpleExpand(){
	var id=$(this).attr('title');
	$('#dmenu_'+id+' > ul').show('slow');
	
	$(this).unbind('click',simpleExpand);
	$(this).click(collapse);
	
	$(this).removeClass('collapsed');
	$(this).addClass('expanded');
	
	return false;
}

/*
function link(){
	location.href = $(this).attr('href');
	return false;
}*/